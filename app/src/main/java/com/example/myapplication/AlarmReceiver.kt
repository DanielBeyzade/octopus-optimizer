package com.example.myapplication

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.preference.PreferenceManager
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*

class AlarmReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {

        var sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        var tesla_username = sharedPref.getString("tesla_username", "Not Set") ?: "Not Set"
        var tesla_password = sharedPref.getString("tesla_password", "Not Set") ?: "Not Set"
        var queue = Volley.newRequestQueue(context)
        val tesla = Tesla(tesla_username, tesla_password, queue)

        tesla.setCustomObjectListener(object : Tesla.TeslaListener {

            override fun onInitComplete() {
                tesla.getVehicleID()
            }

            override fun onGetVehicleIDComplete() {
                tesla.wakeUp()
            }

            override fun onWakeUpComplete() {
                if (tesla.awake)
                {

                    val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
                    var builder = NotificationCompat.Builder(context, "com.example.myapplication")

                        .setContentTitle("Octopus Optimiser")
                        .setContentText("Succesfully woke Tesla to charge")
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setSmallIcon(R.drawable.ic_launcher_foreground)

                        .setContentIntent(pendingIntent)
                        .build();
                    val notificationManager = ContextCompat.getSystemService(
                    context,
                    NotificationManager::class.java
                ) as NotificationManager

                    val channel = NotificationChannel("com.example.myapplication", "Octopus Optimiser",NotificationManager.IMPORTANCE_HIGH)

                    channel.description = "Octopus Optimiser"
                    channel.enableLights(true)
                    channel.enableVibration(true)
                    channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                    notificationManager.createNotificationChannel(channel)

                    notificationManager.notify(0,builder)
                }

                else
                {

                }
            }

            override fun onGetChargeComplete() {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
    })

        tesla.init();
    }
}





