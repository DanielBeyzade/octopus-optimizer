package com.example.myapplication

import java.time.LocalDateTime

class ChargePeriodCost constructor(start_time: LocalDateTime,charge_period: Int,total_cost:Double){
    val start_time : LocalDateTime
    val charge_period : Int
    val total_cost : Double
    val total_cost_string = start_time.toString()
    init {
        this.start_time = start_time

        this.charge_period = charge_period;
        this.total_cost = total_cost;
    }
}