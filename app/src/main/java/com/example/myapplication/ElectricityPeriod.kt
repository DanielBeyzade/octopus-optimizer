package com.example.myapplication

import java.math.BigDecimal
import java.time.LocalDateTime

class ElectricityPeriod constructor(valid_from: LocalDateTime, valid_to: LocalDateTime, value_inc_vat: Float) {
    val valid_from : LocalDateTime
    val valid_to : LocalDateTime
    val value_inc_vat : Float
    var highlightPeriod: Boolean = false



    init {
        this.valid_from = valid_from
        this.valid_to = valid_to
        this.value_inc_vat = value_inc_vat

    }
}