package com.example.myapplication

import android.util.JsonReader
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.JsonRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Float
import java.math.BigDecimal
import java.time.LocalDateTime

class Juicenet constructor(account_token: String, url: String, request_queue: RequestQueue) {

    interface JuicenetListener  {
        // These methods are the different events and
        // need to pass relevant arguments related to the event triggered
        fun onGetDevicesComplete()
        fun onGetStateComplete()

        // or when data has been loaded

    }

    val account_token : String
    var url : String
    val request_queue: RequestQueue
    var unit_id: String
    var token: String
    var amps_limit: Int
    var voltage: Int
    var login_success = false;
    var listener: JuicenetListener? = null;

    init {
        this.account_token = account_token
        this.url = url
        this.request_queue = request_queue
        this.unit_id = ""
        this.token = ""
        this.amps_limit = 0
        this.voltage = 0

    }

    fun setCustomObjectListener(listener: JuicenetListener) {
        this.listener = listener
    }
        fun getDevices()
        {


            val jsonObject = JSONObject()
            jsonObject.put("cmd","get_account_units")
            jsonObject.put("device_id","anything")
            jsonObject.put("account_token",account_token)


            val jsonObjectRequest = JsonObjectRequest(
                Request.Method.POST, url, jsonObject,
                Response.Listener { response ->

                    val success = response.get("success")
                        if (success == true)
                            {
                                login_success = true;
                                val units = response.getJSONArray("units")
                                if (units != null) {

                                    token = units.getJSONObject(0).getString("token")
                                    unit_id = units.getJSONObject(0).getString("unit_id")

                                    Log.d("debug", token.toString())
                                    Log.d("debug", unit_id.toString())
                                    listener?.onGetDevicesComplete()
                                }
                            }





                },
                Response.ErrorListener { error ->
                    Log.d("debug", error.toString())
                })

                request_queue.add(jsonObjectRequest)



        }

    fun getState()
    {
        url = "https://jbv1-api.emotorwerks.com/box_api_secure"
        val jsonObject = JSONObject()
        jsonObject.put("cmd","get_state")
        jsonObject.put("account_token",account_token)
        jsonObject.put("device_id","anything")
        jsonObject.put("token",token)


        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, url, jsonObject,
            Response.Listener { response ->
                Log.d("debug", response.toString())
                val charging = response.getJSONObject("charging")
                amps_limit = charging.getString("amps_limit").toInt()
                voltage = charging.getString("voltage").toInt()
                listener?.onGetStateComplete()



            },
            Response.ErrorListener { error ->
                Log.d("debug", error.toString())
            })

        request_queue.add(jsonObjectRequest)

    }

    fun updateTOU(start:Int,end:Int,ready_by:Int) {

        val base = JSONObject()
        base.put("cmd","set_schedule")
        base.put("device_id","anything")
        base.put("token",token)
        base.put("type","custom")
        base.put("account_token",account_token)

        val weekday = JSONObject()
        weekday.put("start",start)
        weekday.put("end",end)
        weekday.put("car_ready_by",0)


        val weekend = JSONObject()
        weekend.put("start",start)
        weekend.put("end",end)
        weekend.put("car_ready_by",0)

        base.put("weekday",weekday)
        base.put("weekend",weekend)


        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, url, base,
            Response.Listener { response ->
                Log.d("debug", response.toString())






            },
            Response.ErrorListener { error ->
                Log.d("debug", error.toString())
            })

        request_queue.add(jsonObjectRequest)

    }

}