package com.example.myapplication


import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Float.parseFloat
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {


    var halfHourlyPricing = ArrayList<ElectricityPeriod>()
    var chargePeriodCosts: ArrayList<ChargePeriodCost> = ArrayList()
    var kwRequired = 15.5;
    var kwChargeRate: Double = 0.00
    var estimateChargeTime = kwRequired;
    var charge_start_hour: Int = 0
    var charge_start_min: Int = 0
    var charge_start_time: Int = 0
    lateinit var charge_must_complete_by: LocalDateTime
    lateinit var charge_start_datetime: LocalDateTime
    var charge_end_time: Int = 0
    val charge_ready_time: Int = 0
    var charge_periods_required: Int = 0
    lateinit var queue:RequestQueue

    private var alarmMgr: AlarmManager? = null
    private lateinit var alarmIntent: PendingIntent

    lateinit var sharedPref: SharedPreferences
    lateinit var tesla_username: String
    lateinit var tesla_password: String
    lateinit var juicenet_api_key: String

    lateinit var juiceNet: Juicenet
    lateinit var button: FloatingActionButton

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_example, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId
        if (id == R.id.settings) {
            val intent = Intent(this@MainActivity, SettingsActivity::class.java)
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main);

        queue = Volley.newRequestQueue(this)

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        tesla_username = sharedPref.getString("tesla_username", "Not Set") ?: "Not Set"
        tesla_password = sharedPref.getString("tesla_password", "Not Set") ?: "Not Set"
        juicenet_api_key = sharedPref.getString("juicenet_api_key", "Not Set") ?: "Not Set"

        charge_must_complete_by = LocalDateTime.of(LocalDate.now().year,LocalDate.now().month,LocalDateTime.now().dayOfMonth,9,0,0,0)
        charge_must_complete_by = charge_must_complete_by.plusDays(1);
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = MainAdapter(halfHourlyPricing)
        val url =
            "https://api.octopus.energy/v1/products/AGILE-18-02-21/electricity-tariffs/E-1R-AGILE-18-02-21-B/standard-unit-rates/"

        button = findViewById(R.id.button) as FloatingActionButton


        button.setOnClickListener {
            juiceNet.updateTOU(charge_start_time, charge_end_time, 0)
            Toast.makeText(this@MainActivity, "Setting Charger", Toast.LENGTH_SHORT).show()


            val alarmMgr = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmIntent = Intent(this, AlarmReceiver::class.java).let { intent ->
                PendingIntent.getBroadcast(this, 0, intent, 0)
            }


            val calendar: Calendar = Calendar.getInstance().apply {

                set(Calendar.HOUR_OF_DAY, charge_start_datetime.hour)
                set(Calendar.MINUTE,charge_start_datetime.minute)
                set(Calendar.SECOND, 0)
                set(Calendar.YEAR, charge_start_datetime.year)
                set(Calendar.MONTH, charge_start_datetime.monthValue-1)
                set(Calendar.DATE, charge_start_datetime.dayOfMonth)
            }




            alarmMgr.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);



        }


        val queue = Volley.newRequestQueue(this)
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            Response.Listener { response ->
                Log.d("debug", response.toString());
                val pricing = response.getJSONArray("results")


                for (i in 0 until pricing.length() - 40) {

                    val valid_from_string = pricing.getJSONObject(i).getString("valid_from")


                    val valid_to_string = pricing.getJSONObject(i).getString("valid_to")


                    val isoFormatter = DateTimeFormatter.ISO_INSTANT
                    val dateInstantFrom: Instant = Instant.from(isoFormatter.parse(valid_from_string))
                    val dateInstantTo: Instant = Instant.from(isoFormatter.parse(valid_from_string))
                    val valid_from = LocalDateTime.ofInstant(dateInstantFrom, ZoneId.of(("Europe/London")))
                    val valid_to = LocalDateTime.ofInstant(dateInstantTo, ZoneId.of(("Europe/London")))





                    val value_string =
                        parseFloat(pricing.getJSONObject(i).getString("value_inc_vat"))

                    halfHourlyPricing.add(ElectricityPeriod(valid_from, valid_to, value_string))

                }

                halfHourlyPricing.reverse()




                queue.addRequestFinishedListener(RequestQueue.RequestFinishedListener<StringRequest> {


                    setupJuicenet()
                })


            },
            Response.ErrorListener { error ->
                Log.d("debug", error.toString());
            }


        )

        queue.add(jsonObjectRequest);


    }

    fun setupTesla()
    {
        val tesla = Tesla(tesla_username, tesla_password, queue)
        statusTV. text = "Connecting to Tesla"
        tesla.setCustomObjectListener(object : Tesla.TeslaListener {

            override fun onInitComplete() {
                tesla.getVehicleID()
            }

            override fun onGetVehicleIDComplete() {
                statusTV. text = "Waking Tesla"
                tesla.wakeUp()
            }

            override fun onWakeUpComplete() {
                statusTV. text = "Getting Tesla Data"
                if (tesla.awake)
                {
                    tesla.getCharge()
                }

                else
                {
                    statusTV.text = "Couldn't wake Tesla";
                }
            }

            override fun onGetChargeComplete() {
                statusTV. text = "Calculating"
                vehicleChargeTV.text = "Current charge: "+ tesla.battery_level.toString() + "%"
                vehicleRangeTV.text = tesla.battery_range.toInt().toString() + " miles"
                targetSOCTV.text = "Target charge: "+ tesla.charge_limit_soc.toString() + "%"
                vehicleNameTV.text = tesla.vehicle_name

                kwRequired = ((79.50/100*tesla.charge_limit_soc) - (79.50 /100 * tesla.battery_level))
                estimateChargeTime = kwRequired / kwChargeRate;
                chargeTimeTV.text = "Est. Time: " +  BigDecimal(estimateChargeTime).setScale(2, RoundingMode.HALF_EVEN).toString() + " hours";
                charge_periods_required = (estimateChargeTime * 2).toInt()
                requiredkwTV.text = "Charge Needed: " + BigDecimal(kwRequired).setScale(2, RoundingMode.HALF_EVEN).toString() + "KW";
                var lastslot = charge_must_complete_by.plusMinutes(charge_periods_required.times(-30).toLong())
                lastslot   =  lastslot.plusSeconds(1                )//for compareeeee
                for (i in 0 until halfHourlyPricing.size - charge_periods_required)  {


                    var totalcost: Double = 0.00;

                    for (r in 0 until charge_periods_required) {
                        totalcost =
                            halfHourlyPricing[i + r].value_inc_vat.times(kwChargeRate.toFloat().div(2)) + totalcost



                    }


                    if (halfHourlyPricing[i].valid_from.isBefore(lastslot))
                    //Log.d("debug", totalcost.toString());
                    chargePeriodCosts.add(
                        ChargePeriodCost(
                            halfHourlyPricing[i].valid_from,
                            charge_periods_required,
                            totalcost
                        )
                    )


                }

                val best = chargePeriodCosts.minBy { it.total_cost }


                for (r in 0 until charge_periods_required) {
                    halfHourlyPricing[chargePeriodCosts.indexOf(best) + r].highlightPeriod =
                        true
                }

                Log.d(
                    "debug",
                    "Best cost will be " + best?.total_cost.toString() + " at " + best?.total_cost_string
                );
                var cost:Double? = best?.total_cost?.div(100)



                var start = best?.start_time
                charge_start_datetime = start?: LocalDateTime.now();
                var start_int = start?.hour.toString()
                charge_start_time = start_int.toInt() * 60
                charge_start_hour = start_int.toInt();

                var start_int_min = start?.minute.toString()

                charge_start_time = start_int.toInt() * 60
                charge_end_time = (charge_periods_required*60).div(2)
                charge_end_time = charge_end_time + charge_start_time + 30;

                if (charge_end_time > 1440) // check if stop time is out becuase charge started before midnight
                {
                    charge_end_time = charge_end_time - 1440
                }

                if (start_int_min == "30")
                {
                    charge_start_time = charge_start_time + 30
                    charge_end_time = charge_end_time +30
                    charge_start_min = 30;
                }


                statusTV.text = "Estimated Cost: £" + BigDecimal(cost?.toString()).setScale(2, RoundingMode.HALF_EVEN);
                recyclerView.smoothScrollToPosition(chargePeriodCosts.indexOf(best))
                button.visibility = View.VISIBLE;

            }

        });


        tesla.init()
    }

    fun setupJuicenet()
    {
        juiceNet = Juicenet(juicenet_api_key,"https://jbv1-api.emotorwerks.com/box_pin",queue)
        statusTV. text = "Connecting to Juicenet"
        juiceNet.setCustomObjectListener(object : Juicenet.JuicenetListener {

            override fun onGetDevicesComplete() {
                if (juiceNet.login_success)
                {
                    juiceNet.getState()
                }
                else
                {
                    Log.d(
                        "debug",
                        "not logged into juicenet"
                    );
                }
            }

            override fun onGetStateComplete() {
                kwChargeRate = juiceNet.voltage.toDouble() / juiceNet.amps_limit
                chargeRateTV.text = "Charge Rate: " + BigDecimal(kwChargeRate).setScale(2, RoundingMode.HALF_EVEN).toString() + "KW";
                setupTesla()

            }

        });

        juiceNet.getDevices();

    }













    }




