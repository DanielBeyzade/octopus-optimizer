package com.example.myapplication

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item.view.*
import java.time.format.DateTimeFormatter

class MainAdapter(val halfHourlyPricing:ArrayList<ElectricityPeriod>) : RecyclerView.Adapter<CustomViewHolder>()
{
    override fun getItemCount(): Int {
        return halfHourlyPricing.count()
    }



    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bindItems(halfHourlyPricing[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.list_item,parent,false)
        return CustomViewHolder(cellForRow)
    }


}

class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindItems(period: ElectricityPeriod) {
        val formatter = DateTimeFormatter.ofPattern("E dd MMM u HH:mm")
        val formatter2 = DateTimeFormatter.ofPattern("HH:mm")
        itemView.valid_from.text = period.valid_from.format(formatter).toString() + " - " + period.valid_to.format(formatter2).toString()
        itemView.value_inc_vat.text = period.value_inc_vat.toString() + " pence per KW"

        if (period.highlightPeriod == true)
        {
            itemView.valid_from.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorAccent))
            itemView.value_inc_vat.setTextColor(Color.BLACK)
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.colorGrey))
        }

        else
        {
            itemView.valid_from.setTextColor(Color.BLACK)
            itemView.value_inc_vat.setTextColor(Color.BLACK)
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.colorWhite))
        }


    }
}