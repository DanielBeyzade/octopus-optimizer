package com.example.myapplication

import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject

class Tesla constructor(username: String, password: String, request_queue: RequestQueue) {

    interface TeslaListener  {
        // These methods are the different events and
        // need to pass relevant arguments related to the event triggered
       fun onInitComplete()
       fun onGetVehicleIDComplete()
       fun onGetChargeComplete()
       fun onWakeUpComplete()


        // or when data has been loaded

    }
    val username: String
    val password: String
    var request_queue: RequestQueue
    var access_token: String
    var vehicle_id: String
    var vehicle_name: String
    var battery_level: Int
    var battery_range: Float
    var charge_limit_soc: Int
    var listener: TeslaListener? = null;
    var awake: Boolean = false;
    var wakeAttempts: Int = 0;

    init {
        this.username = username
        this.password = password
        this.request_queue = request_queue
        this.access_token = ""
        this.vehicle_id = ""
        this.vehicle_name = ""
        this.battery_level = 0;
        this.battery_range = 0.00f
        this.charge_limit_soc = 0
        this.listener = null;
    }

    fun setCustomObjectListener(listener: TeslaListener) {
        this.listener = listener
    }

    fun wakeUp()
    {
        val jsonObject = JSONObject()



        var url = "https://owner-api.teslamotors.com/api/1/vehicles/" + vehicle_id + "/wake_up"
        val jsonObjectRequest = object : JsonObjectRequest(
            Request.Method.POST, url,jsonObject,
            Response.Listener { response2 ->
                // response
                Log.d("debug", response2.toString())
                Log.d("debug", "Car is awake")
                awake = true;
                listener?.onWakeUpComplete()



            },
            Response.ErrorListener { error ->
                Log.d("debug", error.toString())

                if (error.networkResponse.statusCode == 408 && wakeAttempts < 5) {
                    Log.d("debug", "Car is asleep.....trying to wake")
                    Thread.sleep(1_000)
                    wakeAttempts++
                    //  wakeUp()
                }

                if (wakeAttempts >= 5) {
                    awake = false
                    wakeAttempts = 0;
                    listener?.onWakeUpComplete()
                }


            }


        )


        {
            override fun getHeaders() : Map<String,String> {
                val params = HashMap<String, String>()

                params.put("Authorization", "Bearer " + access_token);
                return params
            }
        }


        request_queue.add(jsonObjectRequest)


    }

    fun init() {
        val jsonObject = JSONObject()
        jsonObject.put("grant_type", "password")
        jsonObject.put(
            "client_id",
            "81527cff06843c8634fdc09e8ac0abefb46ac849f38fe1e431c2ef2106796384"
        )
        jsonObject.put(
            "client_secret",
            "c7257eb71a564034f9419ee651c7d0e5f7aa6bfbd18bafb5c5c033b093bb2fa3"
        )
        jsonObject.put("email", username)
        jsonObject.put("password", password)

        val url = "https://owner-api.teslamotors.com/oauth/token"
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.POST, url, jsonObject,
            Response.Listener { response ->


                Log.d("debug", response.toString())
                access_token = response.getString("access_token")
           //     getVehicleID()
                listener?.onInitComplete()

            },
            Response.ErrorListener { error ->
                Log.d("debug", error.toString())
            })




        request_queue.add(jsonObjectRequest)


    }

    fun getVehicleID()
    {
        val jsonObject = JSONObject()



        val url = "https://owner-api.teslamotors.com/api/1/products"
        val jsonObjectRequest = object : JsonObjectRequest(
            Request.Method.GET, url,jsonObject,
            Response.Listener { response2 ->
                // response
                Log.d("Response", response2.toString())

                val response3 = response2.getJSONArray("response")
                vehicle_id = response3.getJSONObject(0).getString("id")

                vehicle_name = response3.getJSONObject(0).getString("display_name")
                listener?.onGetVehicleIDComplete()

            },
            Response.ErrorListener { error ->
                // TODO Auto-generated method stub
                Log.d("ERROR", "error => $error")
            }


        )


        {
            override fun getHeaders() : Map<String,String> {
                val params = HashMap<String, String>()

                params.put("Authorization", "Bearer " + access_token);
                return params
            }
        }


        request_queue.add(jsonObjectRequest)

    }

    fun getCharge()
    {
        val jsonObject = JSONObject()



        val url = "https://owner-api.teslamotors.com/api/1/vehicles/"+vehicle_id+"/data_request/charge_state"
        val jsonObjectRequest = object : JsonObjectRequest(

            Request.Method.GET, url,jsonObject,
            Response.Listener { response ->
                // response
                Log.d("Response", response.toString())
                val response3 = response.getJSONObject("response")
                battery_level = response3.getString("battery_level").toInt()
                battery_range = response3.getString("battery_range").toFloat()
                charge_limit_soc = response3.getString("charge_limit_soc").toInt()
                listener?.onGetChargeComplete()


            },
            Response.ErrorListener { error ->
                // TODO Auto-generated method stub
                Log.d("ERROR", "error => $error")

                if (error.networkResponse.statusCode == 408)
                {
                    //car is asleep - need to wake it up
                    wakeUp()
                }
            }


        )


        {
            override fun getHeaders() : Map<String,String> {
                val params = HashMap<String, String>()

                params.put("Authorization", "Bearer " + access_token);
                return params
            }
        }


        request_queue.add(jsonObjectRequest)
    }
}